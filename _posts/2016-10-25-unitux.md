---
layout: post
title: UniTux
created: 1477348476
---
Pubblicato un nuovo aggiornamento del "Manuale Operativo per la Community", scaricabile da <a href="/progetti#servizi">questa pagina</a>. In questa occasione è stata introdotta una intera nuova sezione con spunti e suggerimenti su come avviare un nuovo Linux Users Group, con attenzione particolare alla realtà universitaria.

La storia del software libero è infatti strettamente legata al mondo accademico: <a rel="nofollow" href="https://it.wikipedia.org/wiki/Richard_Stallman">Richard Stallman</a> (laureato in Fisica) iniziò la sua battaglia per i diritti digitali sui terminali del celebre MIT, <a rel="nofollow" href="https://it.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a> (laureato in Informatica) presentò il kernel Linux come tesi del suo master, e più in generale tantissimi sono coloro che hanno iniziato ad usare ed apprezzare Linux proprio all'interno della propria facoltà. Oggi però tale contesto, per sua natura popolato da ragazzi e ragazze motivati e desiderosi di nuovi spunti, è invaso ed assediato dai vendor di tecnologie proprietarie, che attirano ed assorbono risorse e competenze al fine di arricchire di applicazioni i propri rispettivi ecosistemi chiusi (e le casse delle proprie rispettive società d'oltreoceano).

Con questa integrazione al "Manuale Operativo" confidiamo di ispirare i giovani delle università italiane affinché instaurino "presìdi linuxari" all'interno delle facoltà (scientifiche, ma non solo!), e si facciano promotori sia delle tecnologie opensource che dei valori di condivisione e collaborazione propri del software libero, tornando ad occupare le aule con corsi, incontri, iniziative divulgative e formative parallele all'offerta accademica, e possano coinvolgere studenti, professori e tecnici.

Per assistenza e supporto in fase di creazione di un nuovo gruppo operativo locale, universitario o meno, <a href="/iscrizione">iscriviti ad Italian Linux Society</a> o più semplicemente mandaci una mail all'indirizzo <a href="mailto:ils-cd@linux.it">ils-cd@linux.it</a>.
