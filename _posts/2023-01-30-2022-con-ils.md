---
layout: post
title: 2022 con ILS
image: /assets/posts/images/2023.jpg
image_copy: <a rel="nofollow" href="https://commons.wikimedia.org/wiki/File:Bratislava_New_Year_Fireworks.jpg">Ondrejk, Public domain, via Wikimedia Commons</a>
---

Il 2022 è stato l'anno della ripresa delle attività dopo la pandemia COVID, ed abbiamo osservato una progressiva crescita di eventi, iniziative ed incontri destinati alla promozione di Linux, del software libero e delle libertà digitali.

<!--more-->

Qui un riassunto di quanto compiuto negli scorsi dodici mesi:

* oltre 6000 euro sono stati distribuiti nel corso dell'anno, in parte a progetti di sviluppo ed in parte a sostegno di iniziative locali per la promozione del software libero. Per il 2023 confidiamo di introdurre qualche novità nel processo di raccolta e distribuzione dei fondi, soprattutto per quanto riguarda i soggetti professionali: restate in ascolto!
* si estende ancora il network ILS: due nuove partnership (l'[adesione a Linux Foundation](/2022/06/30/linux-foundation.html) ed il protocollo di intesa stipulato [con Fondazione Edulife](/2022/11/24/protocollo-fondazione-edulife.html)), una nuova [sezione locale ILS](/sezionilocali) (quella di Milano!), e tre nuove aziende che hanno scelto di [sponsorizzare le nostre attività](/sponsorizzazioni)
* in particolare, il protocollo con Fondazione Edulife è stato utile all'avvio della pianificazione per l'edizione 2023 di MERGE-it, la grande conferenza di incontro e confronto tra le community italiane rivolte alla promozione delle libertà digitali; i lavori sono stati avviati e l'appuntamento è per il 12/13 maggio 2023 a Verona, [sul sito dedicato](https://merge-it.net/) saranno man mano pubblicati gli aggiornamenti
* immancabile il [Linux Day](https://www.linuxday.it/2022/), la maggiore manifestazione italiana di promozione a Linux ed al software libero, che progressivamente - man mano che diminuisce il pericolo COVID - torna ad animare le città in tutta la penisola. Purtroppo abbiamo dovuto rinunciare all'ultimo momento all'edizione online, per problemi tecnici, ma i video sono comunque stati pubblicati sulla [istanza PeerTube di Italian Linux Society](https://video.linux.it/) - insieme a moltissimi altri contenuti creati e condivisi dalla community

Per restare sempre aggiornati su ciò che succede nel mondo del software libero in Italia è possibile sottoscrivere [la nostra newsletter](/newsletter) o seguirci su [Twitter](https://twitter.com/ItaLinuxSociety/), su [Facebook](https://www.facebook.com/italinuxsociety/), su [Instagram](https://www.instagram.com/italinuxsociety/) e su [Mastodon](https://mastodon.uno/@italinuxsociety). Nonché associarsi alla nostra associazione e godere di [tutti i benefici riservati ai soci ILS](/iscrizione).
